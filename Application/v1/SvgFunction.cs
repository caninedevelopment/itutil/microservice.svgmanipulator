﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ITUtil.Microservice.V1
{
    /// <summary>
    /// SvgFunction
    /// </summary>
    public static class SvgFunction
    {
        /// <summary>
        /// UniqueMerge
        /// </summary>
        /// <param name="data">SVG.V1.DTO.SvgImportMerge data</param>
        /// <returns>SVG.V1.DTO.SvgExport</returns>
        public static SVG.V1.DTO.SvgExport UniqueMerge(SVG.V1.DTO.SvgImportMerge data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            List<string> uniqueSVGs = new List<string>();
            foreach (var d in data.svgList)
            {
                uniqueSVGs.Add(Convert(new SVG.V1.DTO.SvgImport() { fileName = data.fileName, whitelistClasses = data.whitelistClasses, svg = d }).svg);
            }

            return Merge(new SVG.V1.DTO.SvgMerge() { fileName = data.fileName, svgList = uniqueSVGs });
        }

        /// <summary>
        /// Merge
        /// </summary>
        /// <param name="data">SVG.V1.DTO.SvgMerge data</param>
        /// <returns>SVG.V1.DTO.SvgExport</returns>
        public static SVG.V1.DTO.SvgExport Merge(SVG.V1.DTO.SvgMerge data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            string svgBase = data.svgList.First();
            int lastindex = svgBase.LastIndexOf("</svg>", StringComparison.OrdinalIgnoreCase);
            string bodystr = svgBase.Substring(0, lastindex); // svg without endtag...
            StringBuilder body = new StringBuilder(bodystr);

            for (int i = 1; i < data.svgList.Count; i++)
            {
                int startindex = data.svgList[i].IndexOf(">", StringComparison.OrdinalIgnoreCase) + 1;
                int endindex = data.svgList[i].LastIndexOf("</svg>", StringComparison.OrdinalIgnoreCase);
                string innerSVG = data.svgList[i].Substring(startindex, endindex - startindex); // svg without svg tag....
                body.Append(innerSVG);
            }

            body.Append("</svg>");

            return new SVG.V1.DTO.SvgExport() { fileName = data.fileName, svg = body.ToString() };
        }

        /// <summary>
        /// Convert
        /// </summary>
        /// <param name="svg">SVG.V1.DTO.SvgImport svg</param>
        /// <returns>SVG.V1.DTO.SvgExport</returns>
        public static SVG.V1.DTO.SvgExport Convert(SVG.V1.DTO.SvgImport svg)
        {
            if (svg == null)
            {
                throw new ArgumentNullException(nameof(svg));
            }

            var classes = CssClasses(svg);
            SVG.V1.DTO.SvgExport res = new SVG.V1.DTO.SvgExport();
            res.fileName = svg.fileName;
            res.svg = svg.svg;

            foreach (var cssClass in classes)
            {
                if (svg.whitelistClasses.Contains(cssClass) == false)
                {
                    var guidClass = Guid.NewGuid().ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture);
                    res.svg = res.svg.Replace(cssClass, "guid_" + guidClass, StringComparison.OrdinalIgnoreCase);
                }
            }

            return res;
        }

        /// <summary>
        /// CssClasses
        /// </summary>
        /// <param name="svg">SVG.V1.DTO.SvgImport svg</param>
        /// <returns>List string</returns>
        public static List<string> CssClasses(SVG.V1.DTO.SvgImport svg)
        {
            if (svg == null)
            {
                throw new ArgumentNullException(nameof(svg));
            }

            List<string> res = new List<string>();

            using (TextReader sr = new StringReader(svg.svg))
            {
                XDocument document = XDocument.Load(sr);
                XElement svg_Element = document.Root;

                IEnumerable<XElement> test = from e1 in svg_Element.Elements("{http://www.w3.org/2000/svg}defs")
                                             select e1;

                StringBuilder sb = new StringBuilder();
                foreach (XElement ee in test)
                {
                    IEnumerable<XElement> test2 = from e2 in ee.Elements("{http://www.w3.org/2000/svg}style")
                                                  select e2;
                    foreach (XElement ee2 in test2)
                    {
                        string classString = ee2.Value;
                        string[] classes = classString.Split('.');
                        foreach (var cssClass in classes)
                        {
                            if (string.IsNullOrEmpty(cssClass) == false)
                            {
                                int endindex = cssClass.IndexOf("{", StringComparison.OrdinalIgnoreCase);
                                res.Add(cssClass.Substring(0, endindex));
                            }
                        }
                    }
                }
            }

            return res;
        }
    }
}