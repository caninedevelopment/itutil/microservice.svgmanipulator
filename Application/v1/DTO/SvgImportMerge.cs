﻿namespace ITUtil.SVG.V1.DTO
{
    using System.Collections.Generic;

    /// <summary>
    /// SvgImportMerge
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class SvgImportMerge
    {
        /// <summary>
        /// Gets or sets fileName
        /// </summary>
        public string fileName { get; set; }

        /// <summary>
        /// Gets or sets svgList
        /// </summary>
        public List<string> svgList { get; set; }

        /// <summary>
        /// Gets or sets whitelistClasses
        /// </summary>
        public List<string> whitelistClasses { get; set; }
    }
}
