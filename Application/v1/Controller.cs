﻿namespace ITUtil.Microservice.V1
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

    /// <summary>
    /// Controller
    /// </summary>
    public class Controller : IMicroservice
    {
        private const string Servicename = "SVG";

        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return Servicename;
            }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("1.0.0.0");
            }
        }

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                   new OperationDescription(
                       "uniqueClasses",
                       "Creates a copy of the SVG, where classes have been renamed to ensure that they are globally unique (ensuring that the SVG can be used within the same document as other SVGs without their styling being mixed",
                       typeof(ITUtil.SVG.V1.DTO.SvgImport),
                       typeof(ITUtil.SVG.V1.DTO.SvgExport),
                       null,
                       ExecuteOperationUniqueClasses),
                   new OperationDescription(
                       "mergeFiles",
                       "Merges a list of SVG documents to become one document",
                       typeof(ITUtil.SVG.V1.DTO.SvgMerge),
                       typeof(ITUtil.SVG.V1.DTO.SvgExport),
                       null,
                       ExecuteOperationMerge),
                   new OperationDescription(
                       "uniqueMergeFiles",
                       "Makes all classes in a SVG documents unique and merges the documents to become one document",
                       typeof(ITUtil.SVG.V1.DTO.SvgImportMerge),
                       typeof(ITUtil.SVG.V1.DTO.SvgExport),
                       null,
                       ExecuteOperationUniqueMerge),
                };
            }
        }

        /// <summary>
        /// ExecuteOperationUniqueClasses
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationUniqueClasses(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var import = MessageDataHelper.FromMessageData<ITUtil.SVG.V1.DTO.SvgImport>(wrapper.messageData);
            var export = SvgFunction.Convert(import);
            return ReturnMessageWrapper.CreateResult(true, wrapper, export, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationMerge
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationMerge(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var import = MessageDataHelper.FromMessageData<ITUtil.SVG.V1.DTO.SvgMerge>(wrapper.messageData);
            var export = SvgFunction.Merge(import);
            return ReturnMessageWrapper.CreateResult(true, wrapper, export, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationUniqueMerge
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationUniqueMerge(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var import = MessageDataHelper.FromMessageData<ITUtil.SVG.V1.DTO.SvgImportMerge>(wrapper.messageData);
            var export = SvgFunction.UniqueMerge(import);
            return ReturnMessageWrapper.CreateResult(true, wrapper, export, LocalizedString.OK);
        }
    }
}
