﻿using ITUtil.Microservice.V1;
using NUnit.Framework;

namespace Unittests
{
    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// GetCssClasses
        /// </summary>
        [Test]
        public void GetCssClasses()
        {
            var svgfiledata = System.IO.File.ReadAllText(".\\TestData\\Test1.svg");
            var import = new ITUtil.SVG.V1.DTO.SvgImport() { fileName = "NA", whitelistClasses = new System.Collections.Generic.List<string>(), svg = svgfiledata };

            var res = SvgFunction.CssClasses(import);

            Assert.AreEqual(1, res.Count);
        }

        /// <summary>
        /// GetCssConvert
        /// </summary>
        [Test]
        public void GetCssConvert()
        {
            var svgfiledata = System.IO.File.ReadAllText(".\\TestData\\Test1.svg");
            var import = new ITUtil.SVG.V1.DTO.SvgImport() { fileName = "NA", whitelistClasses = new System.Collections.Generic.List<string>(), svg = svgfiledata };

            var res = SvgFunction.Convert(import);

            Assert.AreNotEqual(import.svg, res.svg);
        }
    }
}
